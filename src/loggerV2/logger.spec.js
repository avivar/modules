//process.stdout.isTTY = false;
process.env.LOG_LEVEL = 'debug';
require('./logger');
const { getLog } = require('./logger');

describe('Test NEW LOGGER', () => {
    it('Should print logs', (done) => {
        console.error('error log');
        console.warn('warn log');
        console.info('info log');
        console.info('111', '222','3333','info log');
        console.debug('debug log');
        const log = getLog({serviceName: '111', loggerToken: '222'}, 'Internal');
        console.info('asasasdads', log)
        done();
    })
})