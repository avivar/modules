const winston = require('winston');
const { format } = winston;
const { combine, timestamp, printf, colorize } = format;

const env = process.env.NODE_ENV || 'development';
const isProduction = env === 'production';

const LOGGER_TOKEN = 'logger_token';
const SERVICE_NAME = 'service_name';
const MODULE_NAME = 'module_name';

const timestampFormat = new Date().toISOString();

const loggerFormat = printf(({ level, message, timestamp, functionName = '' }) => {
    let loggerFormatObject = {
        "timestamp": timestamp,
        "level": level.toUpperCase(),
        "functionName": functionName,
        "message": message
    }
    return JSON.stringify(loggerFormatObject);
})

const logger = winston.createLogger({
    level: process.env.LOG_LEVEL || 'info',
    exitOnError: false,
    format: combine(
        timestamp(),
        loggerFormat,
        colorize({
            all: !isProduction && process.stdout.isTTY,
        })
    ),
    transports: [
        new winston.transports.Console({
            handleExceptions: true,
            json: isProduction,
            stringify: (obj) => isProduction ? JSON.stringify(obj) : obj,
        }),
    ]
})

if(isProduction){
    console.error = (loggerToken, serviceName, moduleName, ...args) => logger.error.call(
        logger,
        ...args,
        {
            loggerToken: loggerToken || LOGGER_TOKEN,
            serviceName: serviceName || SERVICE_NAME,
            moduleName: moduleName || MODULE_NAME,
        }
    )
    console.warn = (loggerToken, serviceName, moduleName, ...args) => logger.warn.call(
        logger,
        ...args,
        {
            loggerToken: loggerToken || LOGGER_TOKEN,
            serviceName: serviceName || SERVICE_NAME,
            moduleName: moduleName || MODULE_NAME,
        }
    )
    console.info = (loggerToken, serviceName, moduleName, ...args) => logger.info.call(
        logger,
        ...args,
        {
            loggerToken: loggerToken || LOGGER_TOKEN,
            serviceName: serviceName || SERVICE_NAME,
            moduleName: moduleName || MODULE_NAME,
        }
    )
    console.debug = (loggerToken, serviceName, moduleName, ...args) => logger.debug.call(
        logger,
        ...args,
        {
            loggerToken: loggerToken || LOGGER_TOKEN,
            serviceName: serviceName || SERVICE_NAME,
            moduleName: moduleName || MODULE_NAME,
        }
    )
} else {
    console.error = (loggerToken, serviceName, moduleName, ...args) => logger.error.call(
        logger,
        loggerToken || LOGGER_TOKEN,
        serviceName || SERVICE_NAME,
        moduleName || MODULE_NAME,
        ...args,
    )
    console.warn = (loggerToken, serviceName, moduleName, ...args) => logger.warn.call(
        logger,
        loggerToken || LOGGER_TOKEN,
        serviceName || SERVICE_NAME,
        moduleName || MODULE_NAME,
        ...args,
    )
    console.info = (loggerToken, serviceName, moduleName, ...args) => logger.info.call(
        logger,
        loggerToken || LOGGER_TOKEN,
        serviceName || SERVICE_NAME,
        moduleName || MODULE_NAME,
        ...args,
    )
    console.debug = (loggerToken, serviceName, moduleName, ...args) => logger.debug.call(
        logger,
        loggerToken || LOGGER_TOKEN,
        serviceName || SERVICE_NAME,
        moduleName || MODULE_NAME,
        ...args,
    )
}

module.exports = logger;

module.exports.getLog = (options = { loggerToken: LOGGER_TOKEN, serviceName: SERVICE_NAME }, moduleName) => ({
    error: console.error.bind(null, options.loggerToken, options.serviceName, moduleName),
    warn: console.warn.bind(null, options.loggerToken, options.serviceName, moduleName),
    info: console.info.bind(null, options.loggerToken, options.serviceName, moduleName),
    debug: console.debug.bind(null, options.loggerToken, options.serviceName, moduleName),
})
