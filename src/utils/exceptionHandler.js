const logger = require('../logger');

module.exports.catchExceptions = serviceName => {
    process
    .on('unhandledRejection', (err, promise) => {
        const errorStack = JSON.stringify(err.stack || err);
        logger.error(`unhandled rejection in service ${serviceName}; message= ${err.message}; stack trace= ${errorStack.split("\n")}`);
    })
    .on('uncaughtException', (err, promise) => {
        const errorStack = JSON.stringify(err.stack || err);
        logger.error(`uncaught exception in service ${serviceName}; message= ${err.message}; stack trace= ${errorStack.split("\n")}`);
    })
}
