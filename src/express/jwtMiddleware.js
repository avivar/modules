const JWT = require('jsonwebtoken');
const BEARER = "Bearer";
const { StatusCodes } = require('http-status-codes')
const { UNAUTHORIZED } = StatusCodes;
const publicKey = process.env.PUBLIC_KEY;
const logger = require('../logger');

module.exports = (req, res, next) => {
    if(!publicKey){
        logger.error('no PUBLIC_KEY parameter provided');
        return res.status(UNAUTHORIZED).send({});
    }
    const authHeader = req.headers.authorization;
    if(!authHeader){
        logger.error('no authorization header provided');
        return res.status(UNAUTHORIZED).send({});
    }
    const authType = authHeader.split(' ')[0];
    if(authType !== BEARER){
        logger.error(`authorization type ${authType} is not Bearer`);
        return res.status(UNAUTHORIZED).send({});
    }
    const jwtToken = authHeader.split(' ')[1].trim();
    JWT.verify(jwtToken, publicKey, {}, (err, decoded) => {
        if(err){
            logger.error(`error verifying the token; message= ${err.message}`);
            return res.status(UNAUTHORIZED).send({});
        }
        const keys = Object.keys(decoded);
        keys.forEach(key => {
            req.jwtHeaders[key] = decoded[key];
        })
        next();
    })
}