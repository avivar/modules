const urlJoin = require('url-join');

module.exports = (app, version, routeName, ...middlewares) => {
    let path = urlJoin('/', routeName, version);
    app.use(path, ...middlewares);
}