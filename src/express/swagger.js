const swaggerUI = require('swagger-ui-express');

module.exports.withSwagger = (app, swaggerDocument, options = {}) => {
    let swaggerRoute = options.swaggerRoute || '/docs';
    app.use(swaggerRoute, swaggerUI.serve, swaggerUI.setup(swaggerDocument, { }));
}