const withRoutes = require('./routes');
const withSwagger = require('./swagger');

module.exports = {
    withRoutes,
    withSwagger
}
