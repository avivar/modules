const { Summary, Counter } = require('prom-client');
const DEFAULT_SERVICE_NAME = 'default_service';

var metricsHandler;

class PrometheusMetricsHandler {
    constructor(serviceName){
        this.metrics = {};
        this.serviceName = serviceName;
    }

    createMetric(metricName, helpText, labels, isSummary = false){
        let metric = this.getMetric(metricName);
        if(!metric){
            const options = {
                name: metricName,
                helpText,
                labelNames: ["serviceName", ...labels]
            }

            metric = isSummary ? new Summary(options) : new Counter(options);
            this.metrics[metricName] = metric;
        }
    }

    getMetric(metricName){
        return this.metrics[metricName];
    }

    increment(metricName, options){
        const metric = this.getMetric(metricName);
        if(metric){
            options.serviceName = this.serviceName;
            metric.inc(options);
        }
    }
}

const instance = (serviceName) => {
    if(!metricsHandler){
        metricsHandler = new PrometheusMetricsHandler(serviceName || DEFAULT_SERVICE_NAME);
    }
    if(serviceName && metricsHandler.serviceName === DEFAULT_SERVICE_NAME){
        metricsHandler.serviceName = serviceName;
    }
    return metricsHandler;
}

module.exports = {
    instance
}