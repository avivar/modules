const winston = require('winston');
const { format } = winston;
const { combine, timestamp, printf } = format;
const stackTrace = require('stack-trace');
const { json } = require('express');

const loggerFormat = printf(({ level, message, timestamp, functionName = '' }) => {
    let loggerFormatObject = {
        "timestamp": timestamp,
        "level": level.toUpperCase(),
        "functionName": functionName,
        "message": message
    }
    return JSON.stringify(loggerFormatObject);
})

class Logger {
    constructor(){
        this.winstonLogger = winston.createLogger({
            level: process.env.LOG_LEVEL || 'info',
            format: combine(
                timestamp(),
                loggerFormat
            ),
            transports: [
                new winston.transports.Console({
                    handleExceptions: true,
                    json: true
                })
            ]
        })
    }
    debug(message){
        this.writeLog('debug', message);
    }
    info(message){
        this.writeLog('info', message);
    }
    warn(message){
        this.writeLog('warn', message);
    }
    error(message){
        this.writeLog('error', message);
    }
    writeLog(level, message){
        let stackTraceArray = stackTrace.get();
        let functionName = stackTraceArray.length > 2 ? stackTraceArray[2].getFunctionName() : "";
        this.winstonLogger.log(level, message, { functionName });
    }
}

module.exports = new Logger();