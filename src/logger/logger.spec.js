const logger = require('./logger');

describe('Test logger', () => {
    it('Should log debug', (done) => {
        logger.debug('debug log');
        done();
    })
    it('Should log info', (done) => {
        logger.info('info log');
        done();
    })
    it('Should log warn', (done) => {
        logger.warn('warn log');
        done();
    })
    it('Should log error', (done) => {
        logger.error('error log');
        done();
    })
})