const Redis = require('ioredis');
const { v4: UUID } = require('uuid');
const logger = require('../logger');
const REDIS_GET_CALLS = 'redis_get_total_calls';
const REDIS_SET_CALLS = 'redis_set_total_calls';
const REDIS_GET_ERRORS = 'redis_get_total_errors';
const REDIS_SET_ERRORS = 'redis_set_total_errors';
const metricsHandler = require('../metrics/prometheus').instance();

const env = process.env.NODE_ENV || 'development';
const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 6379;

var redisClient;

logger.info(`redis in ${env} mode; host= ${HOST}; port= ${PORT}`);
if(env === 'production'){
    //will use redis sentinel
    redisClient = new Redis({
        host: HOST,
        port: PORT,
        tls: { checkServerIdentity: () => undefined },
        password: process.env.REDIS_PASSWORD,
        name: "mymaster"
    })
} else {
    redisClient = new Redis({
        host: HOST,
        port: PORT
    })
}

metricsHandler.createMetric(REDIS_GET_CALLS, 'counter of redis get calls', []);
metricsHandler.createMetric(REDIS_SET_CALLS, 'counter of redis set calls', []);
metricsHandler.createMetric(REDIS_GET_ERRORS, 'counter of redis get erros calls', ['error']);
metricsHandler.createMetric(REDIS_SET_ERRORS, 'counter of redis set errors calls', ['errro']);

redisClient.on('connect', () => {
    logger.info('redis client connected');
})

redisClient.on('ready', () => {
    logger.info('redis client ready');
})

redisClient.on('close', () => {
    logger.info('redis client connection closed');
})

redisClient.on('reconnecting', () => {
    logger.info('redis client connection reconnecting');
})

redisClient.on('end', () => {
    logger.info('redis client connection end');
})

redisClient.on('error', (err) => {
    logger.error(`redis client connection error; message= ${err.message}`);
})

class RedisClient {
    constructor(prefix = '', keyExpireInSeconds = 0) {
        this.prefix = prefix;
        this.client = redisClient;
        this.keyExpireInSeconds = keyExpireInSeconds;
    }

    getKey(key) {
        return this.prefix ? `${this.prefix}:${key}` : key;
    }

    async asyncGet(key, value, expireInSeconds = this.keyExpireInSeconds) {
        let redisValue;

        try {
            metricsHandler.increment(REDIS_GET_CALLS, {});
            redisValue = await this.client.get(this.getKey(key));
            if (!redisValue) {
                redisValue = await this.client.get(key);
            }
        } catch (err) {
            metricsHandler.increment(REDIS_GET_ERRORS, { error: err });
            throw err;
        }

        try {
            redisValue = JSON.parse(redisValue);
        } catch (err) {
            logger.error(`failed parsing redis value; key= ${key}; error message= ${err.message}`);
        }
        return redisValue;
    }

    async asyncSet(key, value, expireInSeconds = this.keyExpireInSeconds) {
        const redisKey = this.getKey(key);
        let response;

        try {
            metricsHandler.increment(REDIS_SET_CALLS, {});
            if (expireInSeconds) {
                response = await this.client.set(redisKey, JSON.stringify(value), 'EX', expireInSeconds);
            } else {
                response = await this.client.set(redisKey, JSON.stringify(value));
            }
        } catch (err) {
            metricsHandler.increment(REDIS_SET_ERRORS, { error: err });
            throw err;
        }
        return response;
    }

    async asyncTtl (key) {
        const ttl = await this.client.ttl(this.getKey(key));
        return ttl;
    }

    async del (key) {
        let deletedCount;

        try {
            metricsHandler.increment(REDIS_SET_CALLS, {});
            deletedCount = await this.client.del(this.getKey(key));
            if (!deletedCount) {
                deletedCount = await this.client.del(key);
            }
        } catch (err) {
            metricsHandler.increment(REDIS_SET_CALLS, { error: err });
            throw err;
        }
        return deletedCount;
    }

    async asynczadd(key, rank, value, expireInSeconds = this.keyExpireInSeconds) {
        const redisKey = this.getKey(key);
        let response = null;

        try {
            metricsHandler.increment(REDIS_SET_CALLS, {});
            response = await this.client.zadd(redisKey, rank, JSON.stringify(value));
            if (expireInSeconds) {
                await this.client.expire(redisKey, expireInSeconds);
            }
        } catch (err) {
            metricsHandler.increment(REDIS_SET_ERRORS, { error: err });
            throw err;
        }
        return response;
    }

    async asynczrem (key, value) {
        const redisKey = this.getKey(key);
        let response = null;
        
        try {
            metricsHandler.increment(REDIS_SET_CALLS, {});
            response = await this.client.zrem(redisKey, JSON.stringify(value));
        } catch (err) {
            metricsHandler.increment(REDIS_SET_CALLS, { error: err });
            throw err;
        }
        return response;
    }
}

module.exports = RedisClient;